#include <iostream>
using namespace std;

class person {  //parent class:person
  public:
    person ()
      { cout << "person: no parameters\n"; }
    person (int a)
      { cout << "person: int parameter\n"; }
};

class student : public person { //inheriting class:student(class)
  public:
    student (int a)
      { cout << "student: int parameter\n\n";
        char grade [3]; //= {"A","B","C"};
        cout << "The grades are" << grade[] <<endl;
       }
};

class teacher : public person {  //inheriting class:teacher(class)
  public:
    teacher (int a) : person (a)
      { cout << "teacher: int parameter\n\n"; }
};

int main() 
{
  student dela (0);
  teacher dem(0);
  
  system("pause");
  return 0;
  
}
